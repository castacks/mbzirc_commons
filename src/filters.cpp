#include <mbzirc_commons/tools/filters.h>

namespace mbzirc
{
namespace commons
{
  void MedianFilter::reset()
  {
    buff_.clear();
  }

  double MedianFilter::filter(double value)
  {
    std::vector<double> local_buff;        

    // Check if buffer is full
    if (buff_.size() == nbuff_)
    {
      buff_[0] = value;

      // Copy the buffer to sort
      local_buff = buff_;
      std::sort(local_buff.begin(), local_buff.end());
      value = local_buff[local_buff.size() / 2];

      // Shift the buffer
      for (int i = buff_.size() - 1; i > 0; i--)
	buff_[i] = buff_[i-1];
    }
    else
      // Fill the buffer
      buff_.push_back(value); 

    return value;
  }
    
} // end of commons namespace

} // end of mbzirc namespace
    