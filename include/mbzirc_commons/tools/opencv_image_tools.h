#ifndef __OPENCV_IMAGE_TOOLS_H_INCLUDED__
#define __OPENCV_IMAGE_TOOLS_H_INCLUDED__

#include <cv_bridge/cv_bridge.h>

namespace mbzirc
{
namespace commons
{

  class OpenCVImageTools
  {
  public:

    // Prints a text on an image on a specified (bottom-left) position
    static void PrintTextOnImage(cv::Mat img_image, char text[], cv::Point text_bottomleft, cv::Scalar color = cv::Scalar::all(255),
      double font_scale = 0.2, int thickness = 1, int font_face = cv::FONT_HERSHEY_SCRIPT_SIMPLEX);
    
    // Returns the histogram of a given image (converted to grayscale)
    static cv::MatND CalculateHistogram(cv::Mat &img_image, cv::Mat mask = cv::Mat(), 
					bool cumulative = false, int range_start = 0, int range_end = 255);
    
    // Returns the histogram image of a given image (converted to grayscale)
    static cv::Mat HistogramImage(cv::Mat &img_image, int bin_width = 2, int range_start = 0, 
				  int range_end = 255, cv::Mat mask = cv::Mat());
    
    // Crop the image by the rectangle roi
    // Can create a reference to the roi instead of creating new image
    static void CropImage(cv::Mat& src, cv::Mat& dst, cv::Rect roi, bool createReference = false);
    
    /// Rotates image "src" for "angle" radians around point "pt" and put result in the "dst"
    static void RotateImage(cv::Mat& src, cv::Mat& dst, double angle, cv::Point2f pt);

    // Saves an image into a file with a pre-formatted name and number
    static void SaveImageToNumberedFile(cv::Mat img_image, std::string path_and_name, 
					int file_number = 0, std::string replace_string = "$num$");
    
  };

} // end of commons namespace

} // end of mbzirc namespace

#endif // __OPENCV_IMAGE_TOOLS_H_INCLUDED__
