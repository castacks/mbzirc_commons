#include <mbzirc_commons/classes/debug_level.h>

namespace mbzirc
{
namespace commons
{

  bool DebugLevel::SetDebugLevel(int debug_level)
  {
    PrintInfo = debug_level & 1;
    ShowImages = debug_level & 2;
  }

  bool DebugLevel::ShowImages = false;
  bool DebugLevel::PrintInfo = false;

} // end of commons namespace

} // end of mbzirc namespace
