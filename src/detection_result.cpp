#include <mbzirc_commons/classes/detection_result.h>

namespace mbzirc
{
namespace commons
{

  DetectionResult::DetectionResult()
  {
    for (int i = 0; i < num_of_scores_; ++i)
      Scores.push_back(0);
    Tag = -1;
  }

  DetectionResult::DetectionResult(int tag, std::vector<double> scores, cv::RotatedRect resultRect)
  {
    Scores = scores;
    for (int i = scores.size(); i < num_of_scores_; ++i)
      Scores.push_back(0);
    ResultRect = resultRect;
    Tag = tag;
  }

} // end of commons namespace

} // end of mbzirc namespace
