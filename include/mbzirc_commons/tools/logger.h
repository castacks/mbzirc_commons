#ifndef __LOGGER_H_INCLUDED__
#define __LOGGER_H_INCLUDED__ 

#include <string>
#include <cstdio>

#define __COMBINE_LOGGER_VAR_NAMES_HELPER__(X,Y) X##Y 
#define __COMBINE_LOGGER_VAR_NAMES__(X,Y) __COMBINE_LOGGER_VAR_NAMES_HELPER__(X,Y)

#define LOG_DEBUG(args...) ILOG_DEBUG(mbzirc::commons::Logger::Global,args)
#define LOG_INFO(args...) ILOG_INFO(mbzirc::commons::Logger::Global,args)
#define LOG_WARN(args...) ILOG_WARN(mbzirc::commons::Logger::Global,args)
#define LOG_ERROR(args...) ILOG_ERROR(mbzirc::commons::Logger::Global,args)

#define ILOG_DEBUG(varname,args...) \
  {char __COMBINE_LOGGER_VAR_NAMES__(LOG_INFO_,__LINE__) [255]; sprintf(__COMBINE_LOGGER_VAR_NAMES__(LOG_INFO_,__LINE__), args); varname.AddItem(__COMBINE_LOGGER_VAR_NAMES__(LOG_INFO_,__LINE__), "Debug", __func__);}
  
#define ILOG_INFO(varname,args...) \
  {char __COMBINE_LOGGER_VAR_NAMES__(LOG_INFO_,__LINE__) [255]; sprintf(__COMBINE_LOGGER_VAR_NAMES__(LOG_INFO_,__LINE__), args); varname.AddItem(__COMBINE_LOGGER_VAR_NAMES__(LOG_INFO_,__LINE__), "Info", __func__);}

#define ILOG_WARN(varname,args...) \
  {char __COMBINE_LOGGER_VAR_NAMES__(LOG_INFO_,__LINE__) [255]; sprintf(__COMBINE_LOGGER_VAR_NAMES__(LOG_INFO_,__LINE__), args); varname.AddItem(__COMBINE_LOGGER_VAR_NAMES__(LOG_INFO_,__LINE__), "Warn", __func__);}

#define ILOG_ERROR(varname,args...) \
  {char __COMBINE_LOGGER_VAR_NAMES__(LOG_INFO_,__LINE__) [255]; sprintf(__COMBINE_LOGGER_VAR_NAMES__(LOG_INFO_,__LINE__), args); varname.AddItem(__COMBINE_LOGGER_VAR_NAMES__(LOG_INFO_,__LINE__), "Error", __func__);}

#define LOG_INIT(projectname,filename) ILOG_INIT(mbzirc::commons::Logger::Global,projectname,filename)
#define LOG_INIT_UNIQUE(projectname,filename) ILOG_INIT_UNIQUE(mbzirc::commons::Logger::Global,projectname,filename)
#define LOG_INIT_SIMPLE_NUMERIC(filename) ILOG_INIT_SIMPLE_NUMERIC(mbzirc::commons::Logger::Global,filename)
#define LOG_INIT_SIMPLE_NUMERIC_UNIQUE(filename) ILOG_INIT_SIMPLE_NUMERIC_UNIQUE(mbzirc::commons::Logger::Global,filename)

#define ILOG_INIT(varname,projectname,filename) varname.Initialize(projectname,filename,false,true,true,true,true,true,',')
#define ILOG_INIT_UNIQUE(varname,projectname,filename) varname.Initialize(projectname,filename,true,true,true,true,true,true,',')
#define ILOG_INIT_SIMPLE_NUMERIC(varname,filename) varname.Initialize("",filename,false,true,false,false,false,false,' ')
#define ILOG_INIT_SIMPLE_NUMERIC_UNIQUE(varname,filename) varname.Initialize("",filename,true,true,false,false,false,false,' ')

namespace mbzirc
{
namespace commons
{
  class Logger
  {
  public:
    class ILogger 
    { 
    private:
      std::string GetDateTime();
      bool AddToFile(std::string log_item);
      
    public:
      bool Enabled;
      std::string Filename;
      std::string ProjectName;
      bool IncludeTime;
      bool IncludeLogType;
      bool IncludeFunctionName;
      bool IncludeProjectName;
      char Separator;
      bool AddQuotationMark;
      ILogger();
      void Initialize(std::string project_name, std::string filename, bool unique_filename, bool include_time, bool include_projectname, bool include_log_type, 
		  bool include_function_name, bool add_quotation_mark, char separator);
      void AddItem(std::string log_item, std::string log_type, std::string caller_name);
    };
    
    static ILogger Global;
  };
} // end of commons namespace

} // end of mbzirc namespace

#endif // __LOGGER_H_INCLUDED__