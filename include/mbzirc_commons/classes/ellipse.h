#ifndef __ELLIPSE_H_INCLUDED__
#define __ELLIPSE_H_INCLUDED__ 

#include <cv_bridge/cv_bridge.h>

namespace mbzirc
{
namespace commons
{
  
  class Ellipse : public cv::RotatedRect
  {
  private:
    void Initialize();
    
  public:
    
    Ellipse();
    Ellipse(cv::RotatedRect rect);
    bool CreateFromContour(std::vector<cv::Point> &contour, int min_points_num = 5, double max_axis_ratio = -1, int min_axis_size = 0, int max_axis_size = 1000);
    void DrawEllipse(cv::Mat &ellipse_image, cv::Scalar color, bool isFilled = false);
    double OverlapContourOnEllipse(std::vector<cv::Point> &contour, cv::Mat &ellipse_image);
    double OverlapEllipseOnEdgeImage(cv::Mat &ellipse_image, cv::Mat &edge_image);
    
    // Extracts the part of input image specified by the ellipse. Optionally can crop it to the ROI
    bool ExtractEllipseFromImage(cv::Mat &original_image, cv::Mat &ellipse_region, bool crop = false, bool remove_borders = false, int border_size = 3);
    
    // Extracts the part of input image specified by the ellipse. Crops it to the ROI and returns the offset
    bool ExtractEllipseFromImage(cv::Mat &original_image, cv::Mat &ellipse_region, cv::Point &crop_offset);
    
    // Returns the bounding upright rectangle of the ellipse within the given image size
    cv::Rect BoundingRectAdjusted(int image_width, int image_height);
  };

} // end of commons namespace

} // end of mbzirc namespace

#endif // __ELLIPSE_H_INCLUDED__
