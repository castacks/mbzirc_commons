#ifndef __DETECTION_RESULT_H_INCLUDED__
#define __DETECTION_RESULT_H_INCLUDED__ 

#include <vector>
#include <cv_bridge/cv_bridge.h>

namespace mbzirc
{
namespace commons
{
 
  class DetectionResult
  {
  private:
      #define num_of_scores_ 4

  public:
    cv::RotatedRect ResultRect;
    std::vector<double> Scores;
    int Tag;
    
    DetectionResult();
    DetectionResult(int tag, std::vector<double> scores, cv::RotatedRect resultRect);
  };
  
} // end of commons namespace

} // end of mbzirc namespace

#endif // __DETECTION_RESULT_H_INCLUDED__