#include <mbzirc_commons/tools/logger.h>
#include <ros/ros.h>
#include <ios>
#include <fstream>
#include <ctime>

namespace mbzirc
{
namespace commons
{

  Logger::ILogger Logger::Global = Logger::ILogger();

  std::string Logger::ILogger::GetDateTime()
  {
    try
    {
      time_t rawtime;
      struct tm * timeinfo;
      char buffer[80];

      std::time (&rawtime);
      timeinfo = std::localtime(&rawtime);

      std::strftime(buffer,80,"%Y-%m-%d_%H:%M:%S",timeinfo);
      std::string str(buffer);

      return str;
    }
    catch (int e)
    {
      return "";
    }
  }

  bool Logger::ILogger::AddToFile(std::string log_item)
  {
    try
    {
      // Open file stream to append
      std::ofstream log(Filename.c_str(), std::ios_base::app | std::ios_base::out);
      log << log_item << std::endl;
      return true;
    }
    catch (int e)
    {
      return false;
    }
  }
    
  Logger::ILogger::ILogger()
  {
    Enabled = false;
    Filename = "";
    ProjectName = "";
    IncludeTime = true;
    IncludeLogType = true;
    IncludeFunctionName = true;
    IncludeProjectName = true;
    Separator = '.';
    AddQuotationMark = true; 
  }
  
  void Logger::ILogger::Initialize(std::string project_name, std::string filename, bool unique_filename, bool include_time, bool include_projectname, bool include_log_type, 
		bool include_function_name, bool add_quotation_mark, char separator)
  {
    Enabled = true;
    Separator = separator;
    IncludeFunctionName = include_function_name;
    IncludeProjectName = include_projectname;
    IncludeLogType = include_log_type;
    IncludeTime = include_time;
    AddQuotationMark = add_quotation_mark;
    ProjectName = project_name;
    
    if (unique_filename == true)
      filename += "_" + GetDateTime();
    Filename = filename + ".log";
    
//       if (include_log_type || include_function_name)
// 	ILOG_DEBUG("Logger Initialized");
  }

  void Logger::ILogger::AddItem(std::string log_item, std::string log_type, std::string caller_name)
  {
    if (Enabled == false) return;
    
    // Check if filename is not specified yet
    if (Filename.empty())
    {
      ROS_ERROR("Logging error! Filename is not specified yet!");
      return;
    }
    
    std::string log_string = "";
    std::string quotation_mark = AddQuotationMark? "\"" : "";
    
    // Add time
    if (IncludeTime)
    {
      // Convert time to string
      char timestr[50];
      sprintf(timestr, "%0.8lf", ros::Time::now().toSec());
      log_string += quotation_mark + timestr + quotation_mark + Separator;
    }
    
    // Add project name
    if (IncludeProjectName)
      log_string += quotation_mark + ProjectName + quotation_mark + Separator;

    // Add caller function name
    if (IncludeFunctionName)
      log_string += quotation_mark + caller_name + quotation_mark + Separator;

    // Add log type
    if (IncludeLogType)
      log_string += quotation_mark + log_type + quotation_mark + Separator;

    // Add message
    log_string += quotation_mark + log_item + quotation_mark;

    // Save the log item to file
    if (!AddToFile(log_string))
      ROS_ERROR("Logging error! Error writing to file \"%s\"!", Filename.c_str());
  }
  
} // end of commons namespace

} // end of mbzirc namespace
