#ifndef __EDGE_IMAGE_H_INCLUDED__
#define __EDGE_IMAGE_H_INCLUDED__ 

#include <cv_bridge/cv_bridge.h>
#include <vector>

namespace mbzirc
{
namespace commons
{

  class EdgeImage 
  {
  public:
    cv::Mat Image;
    cv::Mat BoldImage;
    std::vector<std::vector<cv::Point> > Contours;
    std::vector<int> GoodContours;
    cv::Mat ContourMap;	// Keeps a map from pixels to corresponding contour number

    /// Calculates edges of the image and its contours
    void CalculateEdges(cv::Mat src, double canny_thresh1 = 50, double canny_thresh2 = 200, double canny_apperture_size = 5, int min_contour_size = 0, bool exclude_small_contours = true, int bold_image_thickness = 2);
  };

} // end of commons namespace

} // end of mbzirc namespace

#endif // __EDGE_IMAGE_H_INCLUDED__
